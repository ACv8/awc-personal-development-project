from IPython.display import clear_output

def display_board(board):
    #create a visual TTT board
    print(board[1]+'|'+board[2]+'|'+board[3])
    print(board[4]+'|'+board[5]+'|'+board[6])
    print(board[7]+'|'+board[8]+'|'+board[9])

test_board = ['#','X','O','X','O','X','O','X','O','X']
display_board(test_board)

def player_input():
    #decide which player is X and O
    '''
    Output = (P1 Marker, P2 Marker)
    '''
    marker = ''
    while not (marker == 'X' or marker == 'O'):
        marker = input("Player 1, Choose 'X' or 'O'").upper()
        
    if marker == 'X':
        return ('X','O')
    else:
        return ('O','X')

(p1marker, p2marker) = player_input()
print(p1marker, p2marker)

def place_marker(board, marker, position):
    #place X or O on the board
    board[position] = marker

place_marker(test_board,'$',8)
display_board(test_board)

def win_check(board, mark):
    #check if a player has won
    return ((board[1] == board[2] == board[3] == mark) or
    (board[4] == board[5] == board[6] == mark) or 
    (board[7] == board[8] == board[9] == mark) or 
    (board[1] == board[4] == board[7] == mark) or 
    (board[2] == board[5] == board[8] == mark) or 
    (board[3] == board[6] == board[9] == mark) or 
    (board[1] == board[5] == board[9] == mark) or 
    (board[3] == board[5] == board[7] == mark))

win_check(test_board,'X')

import random
def choose_first():
    #randomly assign which player goes first
    if random.randint(0, 1) == 0:
        return 'Player 1'
    else:
        return 'Player 2'

def space_check(board, position):
    #check if the chosen position is free
    return board[position] == ' '

def full_board_check(board):
    #check if the board is completely full
    for i in range(1,10):
        if space_check(board,i):
            return False
    return True

def player_choice(board):
    #ask player to put in desired position
    position = 0
    while position not in range(1,10) or not space_check(board, position):
        try:
            position = int(input('Pick your position: (1-9)'))
        except:
            print('Invalid Input')
    return position

def replay():
    #ask if players want to play again
    choice = input('Do you want to play again? Enter Y or N:').upper()
    return choice == 'Y'

#while loop to keep running game
print('Welcome to Tic Tac Toe!')
while True:
    
    #setup board,who goes first, choose X or O
    gboard = [' ']*10
    p1marker, p2marker = player_input()
    turn = choose_first()
    print(turn + ' will go first.')
    play_game = input('Are you ready? y or n')
    
    if play_game == 'y':
        gameplay = True
    else: 
        gameplay = False
        
    while gameplay:
        
        if turn == 'Player 1':
            #player 1 turn
            display_board(gboard)
            position = player_choice(gboard)
            place_marker(gboard,p1marker,position)
            
            if win_check(gboard,p1marker):
                display_board(gboard)
                print('Player 1 wins!')
                gameplay = False
                
            else:
                if full_board_check(gboard):
                    display_board(gboard)
                    print("Tie!")
                    break   
                else:
                    turn = 'Player 2'
        
        else:
            # player 2 turn
            display_board(gboard)
            position = player_choice(gboard)
            place_marker(gboard,p2marker,position)
            
            if win_check(gboard,p1marker):
                
                display_board(gboard)
                print('Player 2 wins!')
                gameplay == False
            else:
                if full_board_check(gboard):
                    display_board(gboard)
                    print("Tie!")
                    break
                else:
                    turn = 'Player 1'
    if not replay():
        break
#break out of while loop on replay